#!/usr/bin/python

import tkinter as tk
import time
import threading
import random
from PIL import Image, ImageTk
from pygame import mixer
import sys, os

# Set correct path for external files
if getattr(sys, 'frozen', False):
    # if you are running in a |PyInstaller| bundle
	extDataDir = sys._MEIPASS
else:
    # we are running in a normal Python environment
	extDataDir = os.getcwd()

DEBUG = False # Enables the debug printouts
# Constants
MASTER_TITLE = "Bee-Bot V4.0"
START_DIRECTION = 3
BLINK_COLOR = 'white'
OVER_TARGET_COLOR = 'maroon1'
FORWARD = '00'
LEFT = '01'
RIGHT = '02'
BACKWARD = '03'
STEP_SIZE_20 = 20
STEP_SIZE_40 = 40
STEP_SIZE_80 = 80
STEP_SIZE = STEP_SIZE_80
START_BUTTON_HEIGHT = 25
WINDOW_TITLE_HIGHT = 10
STATUS_BAR = 30
MATH_BAR = 30
CANVAS_PAD_X = 5
CANVAS_PAD_Y = 6
MAIN_BUTTON_PAD_X = CANVAS_PAD_X+2
RASTER_ADJUST_Y = 3
RASTER_ADJUST_X = 3
TOP_X_GEOMETRY = (6*STEP_SIZE)+(2*CANVAS_PAD_X)+6
TOP_Y_GEOMETRY = (6*STEP_SIZE)+START_BUTTON_HEIGHT+WINDOW_TITLE_HIGHT+ STATUS_BAR+MATH_BAR
TOP_GEOMETRY = '500x500+300+200'
TOP_X = 500
TOP_Y = 500
CANVAS_X = (6*STEP_SIZE)+RASTER_ADJUST_X
CANVAS_Y = (6*STEP_SIZE)+RASTER_ADJUST_Y
CANVAS_BAKGROUND = "gray80" #Gray
CANVAS_BAKGROUND_MATH = 'light steel blue'
NMB_BUTTONS = 4
DIRECTION_MIN = 3
DIRECTION_MAX = 12
DIRECTION_STEP = 3
FRAME_BAKGROUND = 'yellow2'
FRAME_WIDTH = 78
FRAME_HEIGHT = FRAME_WIDTH # Make Beeboot as a square
FRAME_DIFF = FRAME_HEIGHT - FRAME_WIDTH
TARGET_SIZE_20 = 20
TARGET_SIZE = 40 #TARGET_SIZE_20
STOP_COLOR = 'red2'
TARGET_AREA = 100
TARGET_FOUND_COLOR_1 = 'yellow2'
TARGET_FOUND_COLOR_2 = 'black'
BEEBOOT_SPEED = 0.02
LABYRINT_LINE_COLOR = 'red'
OUTSIDE_LABYRINT_COLOR = 'blue'
LSPEED = 2
INIT_STATE = 0
PROGRAMMING_STATE = 1
MOVING_STATE = 2
LABYRINT_STATE = 3
LABYRINT_MOVING_STATE = 4
MATH_STATE = 5
STATUS_PAD = 5
STATUS_FONT = ("Ariel", 9, "bold")
LARGE_FONT = ("Verdana", 9)
BOLD_FONT = ("Ariel", 12, "bold")
BOLD_FONT_MATH = ("Ariel", 15, "bold")

# Global functions
def log(s):
	if DEBUG:
		print(s)

def pos2pix(a):
	return a*STEP_SIZE+(STEP_SIZE/2)

def pix2pos(a):
	return (a-(STEP_SIZE/2))/STEP_SIZE

def playSound(filename):
    mixer.music.load(filename)
    mixer.music.play()

# Global variables
class _zo: pass
zo = _zo()
zo.a = {}
zo.the_way_to_go = []
zo.win_x = 0
zo.win_y = 0
zo.moves = ([[STEP_SIZE, 0], [0, STEP_SIZE], [-STEP_SIZE, 0], [0, -STEP_SIZE]])
zo.new_direction = START_DIRECTION
zo.direction = zo.new_direction
zo.exit_thread = False
zo.stopped_at_target = False
zo.at_border = False
zo.state = INIT_STATE
zo.run_start_begin_now = False
zo.run_check_if_stopped_at_target = False
zo.inside_labyrint = []
zo.outside_labyrint = False
zo.used_coordinates = []
zo.target_order = 1
zo.wrong_target = False
zo.sound_on = False
zo.start_sound = os.path.join(extDataDir, 'start.wav')
zo.stop_sound = os.path.join(extDataDir, 'stop.wav')
zo.goal_sound = os.path.join(extDataDir, 'goal.wav')
zo.labyrint_file = os.path.join(extDataDir, 'Labyrint.dat')
zo.target_picture = os.path.join(extDataDir, 'flower_size.gif')

zo.notes_array = {'e3':	165,	'f3':	175,	'g3':	196,	'a3':	220,	'b3':	247,\
				  'c4':	262,	'd4':	294,	'e4':	330,	'f4':	349,	'g4':	392,\
    			  'a4':	440,	'b4':	494,	'c5':	523,	'd5':	587,	'e5':	659,\
    			  'f5':	698,	'g5':	784,	'a5':	880,	'b5':	988,	'c6':	1047,\
    			  'd6':	1175,	'e6':	1319,	'f6':	1397,	'g6':	1568}
zo.notes = 'g6 g6 g6 c4 c4 g4 g4 a4 a4 g4 f4 f4 e4 e4 d4 d4 c4 g6 c4 c4 g4 g4 a4 a4'.split()

class MathExercises(object):
	def __init__(self, frame, canvas, frame_math):
		self.frame = frame
		self.canvas = canvas
		self.frame = frame_math
		zo.state = MATH_STATE
		self.math_task = tk.StringVar()
		self.term1 = tk.IntVar()
		self.term2 = tk.IntVar()		
		self.canvas.config(background = CANVAS_BAKGROUND_MATH)
		x0,y0=1,1
		for x0 in range(int(CANVAS_X/STEP_SIZE)):
			for y0 in range(int(CANVAS_X/STEP_SIZE)):
				x, y = pos2pix(x0), pos2pix(y0) # make pixel coordinates
				self.canvas.create_text(x, y, text=str(int((6*y0)+x0)), font=BOLD_FONT_MATH, tag='math_text')

		self.space_label = tk.Label(self.frame, width=20)
		self.space_label.pack(side = tk.LEFT)

		self.exercise_label = tk.Label(self.frame, fg ='blue', bg='yellow', text = self.math_task.get(), font=BOLD_FONT_MATH)
		self.exercise_label.pack(side = tk.LEFT, ipadx=5)
		self.result_label = tk.Label(self.frame, font=BOLD_FONT_MATH)
		self.result_label.pack(side = tk.LEFT)
		self.bg_result_label=self.result_label.cget('bg')
		self.change_exercise()
		log(('MathExercises init OUT'))

	def destroy_labels_and_text(self):
		self.space_label.destroy()
		self.exercise_label.destroy()
		self.result_label.destroy()
		self.canvas.delete('math_text')

	def change_exercise(self):
		t1 = t2 =30
		n=0
		
		while (t1 + t2) > 35 and n<100:
			n=n+1
			t1 = random.randint(0, 35)
			t2 = random.randint(0, 10)
			log(('nbm of tries, sum',n, t1 + t2))

		self.term1.set(t1)
		self.term2.set(t2)
		self.show_exercise()

	def show_exercise(self):
		self.result_label.config(text = '', bg=self.bg_result_label)
		self.math_task.set(str(self.term1.get())+'+'+str(self.term2.get())+'=')
		self.exercise_label.config(text = self.math_task.get())

	def check_exercise(self):
		math_result = False
		x = int(pix2pos((STEP_SIZE/2)+zo.win_x-(CANVAS_PAD_X+4)))
		y = int(pix2pos((STEP_SIZE/2)+zo.win_y-(CANVAS_PAD_Y+START_BUTTON_HEIGHT)))
		log(('x, y',x, y))
		log(('sum',self.term1.get()+self.term2.get()))
		result = int((6*y)+x)
		text = self.exercise_label.cget('text')+str(result)
		self.exercise_label.config(text = text)
		if self.term1.get()+self.term2.get() == result:
			log(('result',result))
			self.result_label.config(text = 'RÄTT', bg='green',fg='gold')
			math_result = True
		else:
			self.result_label.config(text = 'FEL', bg='red',fg='black')
			math_result = False

		return math_result


class Target(object):
	def __init__(self, canvas, start_x, start_y):
		self.canvas = canvas
		self.start_x, self.start_y = start_x, start_y
		self.default_image = False
		# Setup target image
		# To resize the image
		#lbimage = Image.open('./flower.gif')
		#lbimage = lbimage.resize((TARGET_SIZE,TARGET_SIZE), Image.ANTIALIAS)
		#lbimage.save('./flower_size.gif')
		try:
			im = Image.open(zo.target_picture)
			self.picture = ImageTk.PhotoImage(im)
			self.target_image = self.canvas.create_image(STEP_SIZE+(STEP_SIZE/2), STEP_SIZE+(STEP_SIZE/2),\
														image=self.picture, state=tk.HIDDEN)

		except IOError as e:
			log(('I/O error({0}): {1}'.format(e.errno, e.strerror)))
			self.default_image = True
			self.target_image = self.canvas.create_oval(32, 32, 32+TARGET_SIZE/2, 32+TARGET_SIZE/2, \
													fill='yellow2', width=6, outline='red', state=tk.HIDDEN)
		# setup two extra targets
		self.target_image_1 = self.canvas.create_oval(32, 32, 32+TARGET_SIZE/2, 32+TARGET_SIZE/2, \
													fill='saddle brown', width=4, outline='yellow2', state=tk.HIDDEN)
		self.target_text_1 = self.canvas.create_text(32+TARGET_SIZE/4, 32+TARGET_SIZE/4, text="1", \
													font=BOLD_FONT, fill='white', state=tk.HIDDEN)
		self.target_image_2 = self.canvas.create_oval(32, 32, 32+TARGET_SIZE/2, 32+TARGET_SIZE/2, \
													fill='yellow2', width=4, outline='red', state=tk.HIDDEN)
		self.target_text_2 = self.canvas.create_text(32+TARGET_SIZE/4, 32+TARGET_SIZE/4, text="2", \
													font=BOLD_FONT, state=tk.HIDDEN)

	def place_target(self):
		if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE: return 0, 0
		while True:
			x0 = random.randint(0, int(CANVAS_X/STEP_SIZE)-1)
			y0 = random.randint(0, int(CANVAS_X/STEP_SIZE)-1)
			# Check if target and frame are at the same place
			if not (x0 == int((self.start_x-(CANVAS_PAD_X+1))/STEP_SIZE) and \
					y0 == int((self.start_y-(START_BUTTON_HEIGHT + CANVAS_PAD_Y))/STEP_SIZE)): break
		return x0, y0

	def show_target(self, x0, y0, target_number):
		# target_number = 0 hide all targets
		# x0, y0 input are step coordinates from 0 to 5
		x, y = pos2pix(x0), pos2pix(y0) # make pixel coordinates
		if target_number == 1:
			self.canvas.coords(self.target_image_1, x-(TARGET_SIZE/4)+RASTER_ADJUST_X, \
									y-(TARGET_SIZE/4)+RASTER_ADJUST_Y, x+(TARGET_SIZE/4)+RASTER_ADJUST_X,\
									y+(TARGET_SIZE/4)+RASTER_ADJUST_Y)
			self.canvas.itemconfig(self.target_image_1, state=tk.NORMAL)
			self.canvas.coords(self.target_text_1, x+RASTER_ADJUST_X, y+RASTER_ADJUST_Y)
			self.canvas.itemconfig(self.target_text_1, state=tk.NORMAL)
		elif target_number == 2:
			self.canvas.coords(self.target_image_2, x-(TARGET_SIZE/4)+RASTER_ADJUST_X, \
									y-(TARGET_SIZE/4)+RASTER_ADJUST_Y, x+(TARGET_SIZE/4)+RASTER_ADJUST_X,\
									y+(TARGET_SIZE/4)+RASTER_ADJUST_Y)
			self.canvas.itemconfig(self.target_image_2, state=tk.NORMAL)
			self.canvas.coords(self.target_text_2, x+RASTER_ADJUST_X, y+RASTER_ADJUST_Y)
			self.canvas.itemconfig(self.target_text_2, state=tk.NORMAL)
		elif target_number == 3:
			if self.default_image:
				self.canvas.coords(self.target_image, x-(TARGET_SIZE/4)+RASTER_ADJUST_X, \
									y-(TARGET_SIZE/4)+RASTER_ADJUST_Y, x+(TARGET_SIZE/4)+RASTER_ADJUST_X,\
									y+(TARGET_SIZE/4)+RASTER_ADJUST_Y)
			else:
				self.canvas.coords(self.target_image, x, y)
			self.canvas.itemconfig(self.target_image, state=tk.NORMAL)
		else:
			self.canvas.itemconfig(self.target_image_1, state=tk.HIDDEN)
			self.canvas.itemconfig(self.target_text_1, state=tk.HIDDEN)
			self.canvas.itemconfig(self.target_image_2, state=tk.HIDDEN)
			self.canvas.itemconfig(self.target_text_2, state=tk.HIDDEN)
			self.canvas.itemconfig(self.target_image, state=tk.HIDDEN)



class Labyrint(object):
	def __init__(self, canvas, frame):
		self.canvas = canvas
		self.roof_line = {}
		self.left_wall_line = {}
		self.move_table = {'FORWARD':FORWARD, 'BACKWARD':BACKWARD, 'LEFT':LEFT, 'RIGHT':RIGHT}
		count_line = 0
		# Make all labyrint lines hidden
		for y in range(int(CANVAS_X/STEP_SIZE)+1):
			for x in range(int(CANVAS_Y/STEP_SIZE)+1):
				labyrint_y_pixel = y * STEP_SIZE
				labyrint_x_pixel = x * STEP_SIZE
				self.roof_line[count_line] = self.canvas.create_line(labyrint_x_pixel + RASTER_ADJUST_X, \
											labyrint_y_pixel + RASTER_ADJUST_Y,\
											labyrint_x_pixel + STEP_SIZE+RASTER_ADJUST_X, labyrint_y_pixel + RASTER_ADJUST_Y, \
											fill=LABYRINT_LINE_COLOR, width=2, state=tk.HIDDEN)
				self.left_wall_line[count_line] = self.canvas.create_line(labyrint_x_pixel + RASTER_ADJUST_X, \
											labyrint_y_pixel + RASTER_ADJUST_X,\
											labyrint_x_pixel + RASTER_ADJUST_X, labyrint_y_pixel + STEP_SIZE + RASTER_ADJUST_Y, \
											fill=LABYRINT_LINE_COLOR, width=2, state=tk.HIDDEN)
				count_line += 1
		self.read_config_file()
		

	def read_config_file(self):
		''' read config file (Labyrint.dat) '''
		self.list_of_labyrints = [['FORWARD', 'RIGHT', 'FORWARD', 'FORWARD', 'LEFT', \
									'FORWARD', 'FORWARD', 'FORWARD', 'FORWARD']]
		a_line = ''
		try:
			with open(zo.labyrint_file, 'r') as fo:
				self.list_of_labyrints = {}
				listan = fo.readlines()
				lab = 0
				for s in listan:
					if s[0] == 'L':
						s = ''.join(s.split())
						w = s.find('=')
						a_line = s[w+1:]
						self.list_of_labyrints[lab] = a_line.split(',')
						lab += 1
			fo.close()
		except IOError as e:
			log(('I/O error({0}): {1}'.format(e.errno, e.strerror)))
		#log(self.list_of_labyrints)

	def erase_labyrint(self):
		# Make all labyrint lines hidden
		count_line = 0
		zo.outside_labyrint = False
		
		for _ in range(int(CANVAS_X/STEP_SIZE)+1):
			for _ in range(int(CANVAS_Y/STEP_SIZE)+1):
				self.canvas.itemconfig(self.roof_line[count_line], state=tk.HIDDEN)
				self.canvas.itemconfig(self.left_wall_line[count_line], state=tk.HIDDEN)
				count_line += 1



	def make_labyrint_border(self):
		self.erase_labyrint()
		the_labyrint = []
		coordinates = []
		zo.outside_labyrint = False
		zo.inside_labyrint = []
		
		nmb_labyrints = random.randint(0, len(self.list_of_labyrints)-1)
		log(('nmb_labyrints', nmb_labyrints))
		for m in self.list_of_labyrints[nmb_labyrints]:
			the_labyrint.append(self.move_table[m])

		y_coordinate = 0 # Y position 0 to 5
		x_coordinate = 0 # X position 0 to 5
		labyrint_direction = 3
		self.canvas.itemconfig(self.roof_line[(y_coordinate*7) + x_coordinate], state=tk.NORMAL)
		self.canvas.itemconfig(self.roof_line[((y_coordinate+1)*7) + x_coordinate], state=tk.NORMAL)
		for n in the_labyrint:
			pre_labyrint_direction = labyrint_direction
			if n == FORWARD:
				if labyrint_direction == 3: x_coordinate += 1 # X position 0 to 5
				elif labyrint_direction == 9: x_coordinate -= 1 # X position 0 to 5
				elif labyrint_direction == 6: y_coordinate += 1 # Y position 0 to 5
				elif labyrint_direction == 12: y_coordinate -= 1 # Y position 0 to 5

				if labyrint_direction == 3 or labyrint_direction == 9:
					for n in range(2):
						self.canvas.itemconfig(self.roof_line[((y_coordinate+n)*7) + x_coordinate], state=tk.NORMAL)
				elif labyrint_direction == 6 or labyrint_direction == 12:
					for n in range(2):
						self.canvas.itemconfig(self.left_wall_line[(y_coordinate*7) + \
												x_coordinate+n], state=tk.NORMAL)

			elif n == RIGHT:
				if labyrint_direction == 12: labyrint_direction = 3
				else: labyrint_direction += 3
				self.make_turn_line(labyrint_direction, x_coordinate, y_coordinate, pre_labyrint_direction)
			elif n == LEFT:
				if labyrint_direction == 3: labyrint_direction = 12
				else: labyrint_direction -= 3
				self.make_turn_line(labyrint_direction, x_coordinate, y_coordinate, pre_labyrint_direction)
			coordinates = (x_coordinate, y_coordinate)
			zo.inside_labyrint.append(coordinates)
			log(('inside labyrint', zo.inside_labyrint[len(zo.inside_labyrint)-1] ))
		return (x_coordinate, y_coordinate)



	def make_turn_line(self, direction, x, y, pre_direction):
		dir = int((direction/3) - 1)
		pre_dir = int((pre_direction/3) - 1)
		dir_line = [1, 7, 0, 0]
		if direction == 3 or direction == 9:
			self.canvas.itemconfig(self.left_wall_line[(y*7) + x + dir_line[dir]], state=tk.HIDDEN)
		elif direction == 6 or direction == 12:
			self.canvas.itemconfig(self.roof_line[(y*7) + x + dir_line[dir]], state=tk.HIDDEN)

		if pre_direction == 3 or pre_direction == 9:
			self.canvas.itemconfig(self.left_wall_line[(y*7) + x + dir_line[pre_dir]], state=tk.NORMAL)
		elif pre_direction == 6 or pre_direction == 12:
			self.canvas.itemconfig(self.roof_line[(y*7) + x + dir_line[pre_dir]], state=tk.NORMAL)

class App(object):
	def __init__(self, master):
		self.text = '→','↓','←','↑','→','↓','←','↑'
		self.text3 = 'Ꙩ\nꙨ','','',''
		self.o_x0 = 0
		self.o_y0 = 0
		self.start_x = CANVAS_PAD_X+4
		self.start_y = START_BUTTON_HEIGHT+ CANVAS_PAD_Y
		self.color = "blue", "red", "green", "yellow", "gray"
		self.color2 = "yellow2", "black", "black", "yellow2" #"orange","dark violet","navy","red"
		self.text2 = 'Ꙩ Ꙩ','','',''
		

		#                            LEFT,EAST               LEFT,SOUTH     LEFT,WEST   LEFT,NORTH
		self.beebots_turn_border = ([[FRAME_DIFF,-FRAME_DIFF],[0,FRAME_DIFF] , [0,0] , [-FRAME_DIFF,0]\
		#							RIGHT,EAST     RIGHT,SOUTH            RIGHT,WEST     RIGHT,NORTH
								,[FRAME_DIFF,0],[-FRAME_DIFF,FRAME_DIFF],[0,-FRAME_DIFF],[0,0]])

		#                            LEFT,EAST                    LEFT,SOUTH                  LEFT,WEST                     LEFT,NORTH
		self.beebots_turn_middle = ([[FRAME_DIFF/2,-FRAME_DIFF/2],[-FRAME_DIFF/2,FRAME_DIFF/2],[FRAME_DIFF/2,-FRAME_DIFF/2],[-FRAME_DIFF/2,FRAME_DIFF/2] \
		#                            RIGHT,EAST                   RIGHT,SOUTH                 RIGHT,WEST                    RIGHT,NORTH
								,[FRAME_DIFF/2,-FRAME_DIFF/2],[-FRAME_DIFF/2,FRAME_DIFF/2],[FRAME_DIFF/2,-FRAME_DIFF/2],[-FRAME_DIFF/2,FRAME_DIFF/2]])
		#						EAST (3)					SOUTH(6)					WEST(9)						NORTH(12)
		self.buttons_place=[[tk.RIGHT, tk.BOTTOM,tk.TOP],[tk.BOTTOM, tk.LEFT,tk.RIGHT],[tk.LEFT, tk.TOP,tk.BOTTOM],[tk.TOP, tk.RIGHT,tk.LEFT]]
		self.button_frame = []
		self.name_text= [['\nA\nL','V\nE'],['VE      ','     AL'],['V\nE','\nA\nL'],['     AL','VE      ']]
		self.should_repeat = True
		self.target_image = 0
		
		
		self.v = tk.IntVar(value=1)

		# Type of Beebots turning
		zo.beebots_turn = self.beebots_turn_border # Fill in the type of Beebots turning border or middle<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		master.title(MASTER_TITLE)

        #create canvas
		self.canvas1 = tk.Canvas(master, relief = tk.FLAT, background = CANVAS_BAKGROUND, width = CANVAS_X, height = CANVAS_Y)
		
		#log(self.canvas1.winfo_geometry())
       	# Create lines
		nmb_raster_lines = CANVAS_X/STEP_SIZE
		for n in range (int(nmb_raster_lines)+1):
			self.canvas1.create_line(0, (n*STEP_SIZE)+RASTER_ADJUST_Y , CANVAS_X, (n * STEP_SIZE)+RASTER_ADJUST_Y, fill='gray', dash=(1,))
			self.canvas1.create_line((n*STEP_SIZE)+RASTER_ADJUST_X, 6 , (n*STEP_SIZE)+RASTER_ADJUST_X, CANVAS_Y, fill = 'gray', dash = (1,))

		# Create status lables
		frame1 = tk.Frame (master)
		frame1.pack( anchor = tk.N, fill = tk.X, padx = MAIN_BUTTON_PAD_X, side = tk.TOP)
		self.canvas1.pack(side = tk.TOP, anchor = tk.SW, padx = CANVAS_PAD_X, pady = CANVAS_PAD_Y-5)

		# Start target class
		self.target = Target(self.canvas1, self.start_x, self.start_y)

		frame_math = tk.Frame (master)
		frame_math.pack(padx = CANVAS_PAD_X,fill = tk.X, side = tk.TOP)
		frame_status = tk.Frame (master)
		frame_status.pack(padx = CANVAS_PAD_X,fill = tk.X, side = tk.BOTTOM)
		tk.Label (frame_status, text = "Status:", bg = 'lightblue', justify=tk.RIGHT, font=STATUS_FONT, pady=STATUS_PAD).pack(side = tk.LEFT, anchor=tk.N)
		self.sound_label = tk.Label (frame_status, text = "Knapp 'l' för ljud på", bg = 'lightgreen', justify=tk.LEFT,fg='black', width='18', pady=STATUS_PAD)
		self.sound_label.pack(side = tk.LEFT, anchor=tk.N)
		num_targets = tk.Label(frame_status, bg = 'light sky blue')#, width = 15, height = 1 )
		self.num_targets_message = tk.Label(frame_status, bg = 'light sky blue', font=STATUS_FONT, pady=STATUS_PAD)
		# Creating the START button
		self.reset_button = tk.Button (frame1, text = "Börja om", command = self.reset_now).pack(side = tk.RIGHT)
		# Creating the RESET button
		self.start_button = tk.Button (frame1, text = " START", command = self.begin_now).pack(side = tk.LEFT)
		# Creating the Place target button
		self.target_button = tk.Button (frame1, text = "Ny blomma", command = self.main_place_target).pack(side = tk.LEFT,expand = True)
		# Creating the new Place Beeboot start
		self.target_button = tk.Button (frame1, text = "Ny början", command = self.place_new_frame).pack(side = tk.RIGHT,expand = True)
		# Creating a new labyrint
		self.labyrint = Labyrint(self.canvas1, frame_status)
		self.labyrint_button = tk.Button (frame1, text = "Ny labyrint", command =self.enter_labyrint).pack(side = tk.RIGHT,expand = True)
		# Make status label for labyrint
		self.state_label = tk.Label(frame_status, text="ESC återgår till normal", bg='gold', \
										justify=tk.LEFT, fg='gold', pady=STATUS_PAD)
		self.state_label.pack(fill=tk.X, side=tk.LEFT, anchor=tk.N)

		# Pack num_targets after labyrint status
		num_targets.pack(fill = tk.X, side = tk.LEFT)
		self.num_targets_message.pack(fill = tk.X, side = tk.LEFT)		
		tk.Radiobutton(num_targets, text="En", variable=self.v, value=1, bg='light sky blue', command=self.change_number_of_targets).pack(side=tk.LEFT)
		tk.Radiobutton(num_targets, text="Två", variable=self.v, value=2, bg='light sky blue', command=self.change_number_of_targets).pack(side=tk.LEFT)
		tk.Radiobutton(num_targets, text="Tre", variable=self.v, value=3, bg='light sky blue', command=self.change_number_of_targets).pack(side=tk.LEFT)
		# MAke a Math button in status field
		tk.Button (frame_math, text = "Matte", bg = 'white', command = lambda:self.math_button(frame1,self.canvas1,frame_math), pady=3).pack(side = tk.LEFT)
		
	# Creating the Beeboot with the arrows
		self.frame = tk.Frame (master, bg= FRAME_BAKGROUND)

		self.frame_forward = tk.Frame (self.frame, bg= 'yellow2')
		self.frame_forward.pack(fill=tk.BOTH, expand = True)
		self.frame_turns = tk.Frame (self.frame, bg= 'black')
		self.frame_turns.pack(fill=tk.BOTH, expand = True)
		self.frame_backward = tk.Frame (self.frame, bg= 'yellow2')
		self.frame_backward.pack(fill=tk.BOTH, expand = True)

		self.button_frame = [self.frame_forward,self.frame_turns,self.frame_turns,self.frame_backward]
	
		self.place_frame_do_buttons(self.start_x,self.start_y)

		self.frame.bind_all("<Key>",lambda event: self.arrow_key( event,))
		self.frame.bind_all("<KeyRelease>",lambda event: self.arrow_key_release( event,))
		self.frame.bind_all("<Return>",self.begin_now)
		self.frame.bind_all('<Escape>', self.exit_labyrint)
		self.frame.bind_all('l', self.enable_disable_sound)

		# Setup all threads
		threading.Thread(target=App.start_begin_now, args=(None, self.frame, lambda: self.do_buttons_direction(), \
						lambda: self.check_window_border(), lambda: self.check_if_correct_target()),).start()
		threading.Thread(target=App.check_if_stopped_at_target, args=(None, lambda: self.check_window_border(),\
						lambda: self.math.check_exercise()),).start()
		mixer.init()
		
		log(('App init OUT'))

	def math_button(self, frame, canvas, frame_math):
		if zo.state != MATH_STATE:
			if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE:
				self.exit_labyrint(None)
			self.target.show_target(0, 0, 0) # hide all targets
			self.state_label.configure(fg='black')
			self.canvas1.itemconfig(self.target_image, state=tk.HIDDEN)
			self.start_x = CANVAS_PAD_X+4
			self.start_y = START_BUTTON_HEIGHT+ CANVAS_PAD_Y  #Beeboot always att upper left corner
			zo.new_direction = START_DIRECTION
			zo.direction = zo.new_direction	
			zo.the_way_to_go = []
			zo.exit_thread = True
			zo.stopped_at_target = False
			zo.at_border = False
			self.place_frame_do_buttons(self.start_x,self.start_y)
			self.math = MathExercises(frame, canvas, frame_math)
		else:
			self.math.change_exercise()
			


	def change_number_of_targets (self):
		if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE: return
		self.target.show_target(x, y, 0) # hide targets
		self.num_targets_message.configure(text='')

	def main_place_target (self):
		if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE or zo.state == MATH_STATE: return	
		zo.used_coordinates = []
		x, y = 0,0 # no  coordinates is needed when hide targets
		self.target.show_target(x, y, 0) # hide targets
		self.num_targets_message.configure(text='')
		
		if self.v.get() > 1:
			x, y = self.check_used_coordinates(zo.used_coordinates)
			zo.used_coordinates.append ((x, y))
			self.target.show_target(x, y, 1)
		if self.v.get() > 2:
			x, y = self.check_used_coordinates(zo.used_coordinates)
			zo.used_coordinates.append ((x, y))
			self.target.show_target(x, y, 2)

		x, y = self.check_used_coordinates(zo.used_coordinates) # place and show flower target
		zo.used_coordinates.append ((x, y))
		self.target.show_target(x, y, 3)
		log (('coordinate', zo.used_coordinates))

	def check_used_coordinates (self, cordinates):
		while True: 
			x, y = self.target.place_target()
			log (('new_coordinate-1', x, y))
			same_place = 0
			for m, n in cordinates:
				
				if x == m and y == n: same_place += 1
			if same_place == 0: break
		return x, y

	def arrow_key_release (self, event):
		#time.sleep(0.3)
		self.should_repeat = True

	def arrow_key(self, event):
		if self.should_repeat:
			self.should_repeat = False
			if event.keysym == 'Up': key_direction = FORWARD
			elif event.keysym == 'Down': key_direction = BACKWARD
			elif event.keysym == 'Left': key_direction = LEFT
			elif event.keysym == 'Right': key_direction = RIGHT
			else : key_direction = None

			if key_direction and (zo.state == PROGRAMMING_STATE or  zo.state == LABYRINT_STATE):
				lbfg = zo.a[key_direction].cget("fg")
				zo.a[key_direction].config(bg = 'white',fg = 'black',relief = tk.SUNKEN)
				self.frame.after(100, lambda: zo.a[key_direction].config(bg = self.color[int(key_direction)], fg = lbfg, relief = tk.RAISED))
				self.vilken_direction(key_direction)

	def enter_labyrint(self):
		if zo.state == MATH_STATE: return
		log('enter_labyrint IN')
		zo.used_coordinates = []
		target_x, target_y = 0, 0
		self.state_label.configure(fg='black')
		self.canvas1.itemconfig(self.target_image, state=tk.HIDDEN)
		self.start_x = CANVAS_PAD_X+4
		self.start_y = START_BUTTON_HEIGHT+ CANVAS_PAD_Y  #Beeboot always att upper left corner
		zo.new_direction = START_DIRECTION
		zo.direction = zo.new_direction	
		zo.the_way_to_go = []
		zo.exit_thread = True
		zo.stopped_at_target = False
		zo.at_border = False
		zo.state = LABYRINT_STATE
		zo.target_order = 1
		self.place_frame_do_buttons(self.start_x,self.start_y)
		target_x, target_y = self.labyrint.make_labyrint_border()
		log(('target_x, target_y', target_x, target_y))
		zo.used_coordinates.append ((target_x, target_y))
		self.target.show_target(target_x, target_y, 0) # hide all targets
		self.target.show_target(target_x, target_y, 3) # set flower target
		log('enter_labyrint OUT')
	
	def exit_labyrint(self, event):
		if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE:
			self.state_label.configure(fg='gold')
			self.labyrint.erase_labyrint()
			self.reset_now()
			self.target.show_target(x, y, 0) # hide targets
			zo.state = PROGRAMMING_STATE
		elif zo.state == MATH_STATE:
			self.state_label.configure(fg='gold')
			self.reset_now()
			self.math.destroy_labels_and_text()
			zo.state = PROGRAMMING_STATE


	def enable_disable_sound(self, event):
		if zo.sound_on: zo.sound_on = False; self.sound_label.configure(fg='black', text="Knapp 'l' för ljud på")
		else: zo.sound_on = True; self.sound_label.configure(fg='black', text="Knapp 'l' för ljud av")

	def do_buttons (self):
		log("do_buttons IN")
		if zo.state == MOVING_STATE or zo.state == INIT_STATE:
			zo.state = PROGRAMMING_STATE # Possible to program with keys if state was MOVING_STATE
		elif zo.state == LABYRINT_MOVING_STATE:
			zo.state = LABYRINT_STATE
		_dir = int(zo.direction/DIRECTION_STEP)
		self.frame_forward.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM
		self.frame_turns.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM
		self.frame_backward.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM

		for x in [FORWARD,RIGHT,LEFT,BACKWARD]:
			butn = tk.Button(self.button_frame[int(x)], bg=self.color[int(x)], \
							font=LARGE_FONT)
			zo.a[x] = butn

		zo.a[FORWARD].pack(expand = True, fill= tk.BOTH)
		zo.a[FORWARD].config(text=self.text[_dir-1],command = lambda: self.vilken_direction(FORWARD),fg='yellow')
		zo.a[RIGHT].pack(expand = True, fill= tk.BOTH, side = self.buttons_place[_dir-1][1])# NORTH ->RIGHT, EAST -> BOTTOM, WEST -> TOP, SOUTH -> LEFT
		zo.a[RIGHT].config(text=self.text[_dir],command = lambda: self.vilken_direction(RIGHT))
		zo.a[LEFT].pack(expand = True, fill= tk.BOTH, side = self.buttons_place[_dir-1][2])# NORTH ->LEFT, EAST -> TOP, WEST -> BOTTOM, SOUTH -> RIGHT
		zo.a[LEFT].config(text=self.text[_dir+2],command = lambda: self.vilken_direction(LEFT))
		zo.a[BACKWARD].pack(expand = True, fill= tk.BOTH)
		zo.a[BACKWARD].config(text=self.text[_dir+1],command = lambda: self.vilken_direction(BACKWARD))

		log("do_buttons OUT")


	def do_buttons_direction (self):
		log("do_buttons_direction IN")
		if zo.state == PROGRAMMING_STATE:
			zo.state = MOVING_STATE # Possible to program with keys
		elif zo.state == LABYRINT_STATE:
			zo.state = LABYRINT_MOVING_STATE

		self.destroy_boxes()
		_dir = int(zo.direction/DIRECTION_STEP)
		self.frame_forward.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM
		self.frame_turns.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM
		self.frame_backward.pack(side = self.buttons_place[_dir-1][0])# NORTH ->TOP, EAST -> RIGHT, WEST -> LEFT, SOUTH -> BOTTOM

		for x in [FORWARD,RIGHT,LEFT,BACKWARD]:
			butn = tk.Label(self.button_frame[int(x)], bg=self.color2[int(x)], font=LARGE_FONT, text = self.text2[int(x)], relief=tk.FLAT)
			butn.pack(expand = True, fill= tk.BOTH)
			#time.sleep(0.5)
			if x == FORWARD:
				if zo.direction == 9 	: butn.config(text = self.text3[int(x)])
				elif zo.direction == 3 	: butn.config(text = self.text3[int(x)])
			elif x == LEFT				: 
				butn.pack(side = self.buttons_place[_dir-1][2])
				butn.config(text = self.name_text[_dir-1][0],fg='white')
			elif x == RIGHT				: 
				butn.pack(side = self.buttons_place[_dir-1][1])
				butn.config(text = self.name_text[_dir-1][1],fg='white')
			zo.a[x] = butn

		log("do_buttons_direction OUT")

	def vilken_direction(self, vart, event=None):
		zo.the_way_to_go.append(vart)
		log(("vilken_direction: zo.the_way_to_go",zo.the_way_to_go))

	def begin_now(self, event=None):
		if zo.state == PROGRAMMING_STATE or zo.state == LABYRINT_STATE or zo.state == MATH_STATE:
			zo.stopped_at_target = False
			zo.at_border = False
			zo.exit_thread = False
			zo.run_start_begin_now = True # Start thread; App.start_begin_now
			zo.wrong_target = False
			zo.target_order = 1
			self.num_targets_message.configure(text='')
		
	def reset_now(self):
		if zo.state == MATH_STATE:
			self.math.show_exercise()
		zo.the_way_to_go = []
		zo.exit_thread = True
		zo.at_border = False
		zo.stopped_at_target = False
		zo.outside_labyrint = False
		zo.wrong_target = False
		zo.target_order = 1
		self.num_targets_message.configure(text='')
		time.sleep(0.5) # Time for the task to end
		self.place_frame_do_buttons(self.start_x,self.start_y)
		self.canvas1.config(background = CANVAS_BAKGROUND)


	def place_frame_do_buttons (self,x0,y0):#Place the frame at startposition
		self.destroy_boxes()
		#log("1-zo.win_x, zo.win_y = x0,y0",zo.win_x, zo.win_y, x0,y0)
		zo.win_x, zo.win_y = x0,y0
		#time.sleep(1)
		self.frame.place(width = FRAME_WIDTH, height= FRAME_HEIGHT, x = zo.win_x, y = zo.win_y)
		zo.direction = zo.new_direction
		self.do_buttons()


	def destroy_boxes (self):
		#log("knappar", zo.a)
		for x in zo.a:
			zo.a[x].destroy()

	def check_if_correct_target(self):
		if zo.wrong_target == False: # check if targets are collected in correct order
			target_numb = 0
			len_list = len(zo.used_coordinates)
			self.num_targets_message.configure(text='') # erase status text
			for a in zo.used_coordinates:
				target_numb += 1
				if (int((zo.win_x-9)/STEP_SIZE), int((zo.win_y-31)/STEP_SIZE)) == a and\
					len_list != zo.target_order: 
					if target_numb == zo.target_order:
						log(('correct track', zo.target_order))
						self.num_targets_message.configure(text='RÄTT :)', fg='green')
						zo.target_order += 1
					else: 
						zo.wrong_target =True
						self.num_targets_message.configure(text='FEL!! :(', fg='red')
						log(('wrong track', zo.target_order))
					log(('used_coordinates check', a, zo.wrong_target))

	def check_window_border (self): # return True if within target
		log("check_window_border IN")
		# Check end of TOP window
		#w_req, h_req = top.winfo_width(), top.winfo_height()
		w_reqcanvas1, h_reqcanvas1 = self.canvas1.winfo_width(), self.canvas1.winfo_height()
		#log("1-w_req,zo.win_x , offset, new_x",w_req,zo.win_x,-FRAME_WIDTH-CANVAS_PAD_X,w_req-FRAME_WIDTH-CANVAS_PAD_X)
		
		if zo.direction == 12 or zo.direction == 6 :
			if   zo.win_y <= START_BUTTON_HEIGHT+4: zo.win_y = START_BUTTON_HEIGHT+4+2; zo.at_border = True
			elif zo.win_y >= h_reqcanvas1 - FRAME_HEIGHT+START_BUTTON_HEIGHT:
				zo.win_y = h_reqcanvas1-FRAME_HEIGHT+START_BUTTON_HEIGHT-RASTER_ADJUST_Y 
				zo.at_border = True
		else: # direction 3 or 9
			if   zo.win_x <= CANVAS_PAD_X: zo.win_x = CANVAS_PAD_X+4; zo.at_border = True
			elif zo.win_x >= w_reqcanvas1-FRAME_HEIGHT+CANVAS_PAD_X:
				zo.win_x = w_reqcanvas1-FRAME_HEIGHT+CANVAS_PAD_X-RASTER_ADJUST_X
				zo.at_border = True
		# Check if outside labyrint
		if zo.state == LABYRINT_MOVING_STATE:
			innei = False
			for a in zo.inside_labyrint:
				if (int((zo.win_x-9)/STEP_SIZE), int((zo.win_y-31)/STEP_SIZE)) == a: 
					innei = True
					break
			if not innei: zo.outside_labyrint = True
		

		# Check also position from target
		# position for the last target
		target_center_x = pos2pix(0) #default values
		target_center_y = pos2pix(0) # default values
		if len(zo.used_coordinates) > 0: # if no targets is set return False
			for m, n in zo.used_coordinates:
				target_center_x = pos2pix(m)
				target_center_y = pos2pix(n)

			target_xlength = FRAME_HEIGHT
			target_ylength = FRAME_WIDTH
			if zo.direction == 12 or zo.direction == 6 :
				target_xlength = FRAME_WIDTH
				target_ylength = FRAME_HEIGHT

			if zo.win_x + target_xlength-(TARGET_SIZE/2) > (target_center_x+5) >= zo.win_x+(TARGET_SIZE/2):
				if zo.win_y + target_ylength-(TARGET_SIZE/2) >= (target_center_y+31) > zo.win_y+(TARGET_SIZE/2):
					log("return True")
					log("check_window_border OUT")
					return True

		log("check_window_border OUT")
		return False

	def place_new_frame (self):#Place the frame at a new startposition
		if zo.state == LABYRINT_STATE or zo.state == LABYRINT_MOVING_STATE: return
		while True:
			x0 = random.randint(0,int(CANVAS_X/STEP_SIZE)-1)
			y0 = random.randint(0,int(CANVAS_X/STEP_SIZE)-1)
			# Check if target and fram are at the same place
			# << todo <<<<<<<<<<<<<<<<<<< kolla också de övriga målen <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			if not (x0 == int(pix2pos(self.o_x0)) and y0 == int(pix2pos(self.o_y0))): break

		self.start_x, self.start_y = (CANVAS_PAD_X+4) + x0*STEP_SIZE, (START_BUTTON_HEIGHT + CANVAS_PAD_Y) + y0*STEP_SIZE
		zo.new_direction = random.randint(DIRECTION_MIN,DIRECTION_MAX/DIRECTION_STEP)*DIRECTION_STEP
		self.reset_now()

	def start_begin_now(self, frame, f_direction, check_position, check_if_correct_target):
		log(("THREAD-start_begin_now IN %s\n"%threading.current_thread().name ))
		while True:
			while not zo.run_start_begin_now: time.sleep(0.1) # wait until time to run the tread
			bw_lbbg = zo.a[BACKWARD].cget("bg")
			if zo.sound_on:
				playSound(zo.start_sound)
				time.sleep(0.3)
			f_direction()
			for n in zo.the_way_to_go:
			# exit for-loop if flag set
				if zo.exit_thread: break
				_x0 = zo.win_x
				_y0 = zo.win_y
				#log("0- n _x0, zo.win_x",n, _x0, zo.win_x)
				#log("0-n _y0, zo.win_y",n, _y0, zo.win_y)
				lbbg = zo.a[n].cget("bg")
				prev_direction = zo.direction

				# Move the box
				if n == LEFT:
					zo.direction -= DIRECTION_STEP
					if zo.direction < DIRECTION_MIN : zo.direction = DIRECTION_MAX
				elif n == RIGHT:
					zo.direction += DIRECTION_STEP
					if zo.direction > DIRECTION_MAX : zo.direction = DIRECTION_MIN
				elif n == FORWARD:
					#log("FORWARD    zo.direction, zo.moves",zo.direction, zo.moves)
					x,y = zo.moves[int(zo.direction/DIRECTION_STEP)-1]
					#log("x,y", x,y)
					zo.win_x += x
					zo.win_y += y
				else: #BACKWARD
					#log("BACKWARD  zo.direction, zo.moves",zo.direction, zo.moves)
					x,y = zo.moves[int(zo.direction/DIRECTION_STEP)-1]
					zo.win_x -= x
					zo.win_y -= y
				#log("1- _x0, zo.win_x",_x0, zo.win_x)
				#log("1-_y0, zo.win_y", _y0, zo.win_y)
				if n == LEFT or n == RIGHT:
					if zo.direction == 12 or zo.direction == 6 :
						frame.place(width = FRAME_WIDTH, height= FRAME_HEIGHT, x = zo.win_x, y = zo.win_y)
					else:
						frame.place(width = FRAME_HEIGHT, height= FRAME_WIDTH, x = zo.win_x, y = zo.win_y)
					f_direction()
					#Place frame when changing direction
					turn = ((prev_direction/3) + 4*(int(n)-1))-1
					turn_x, turn_y = zo.beebots_turn[int(turn)]
					#log("before zo.win_x, zo.win_y",zo.win_x, zo.win_y)
					zo.win_x +=turn_x
					zo.win_y +=turn_y
					if check_position(): zo.a[FORWARD].config(bg = OVER_TARGET_COLOR)
					#log("turn, n, turn_x, turn_y, zo.win_x, zo.win_y",int(turn),n, turn_x, turn_y, zo.win_x, zo.win_y)
					frame.place(x = zo.win_x, y = zo.win_y)
					time.sleep(0.1) # Some time to turn around visual

				else: #FORWARD and BACKWARD
					if zo.exit_thread: continue
					check_position()  #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<????????
					_x= 1 if _x0 - zo.win_x < 0  else -1
					_y= 1 if _y0 - zo.win_y < 0  else -1
					#log("1-_x, _x0, zo.win_x",_x, _x0, zo.win_x)
					#log("1-_y, _y0, zo.win_y",_y, _y0, zo.win_y)
					for _ in range(int(abs(_x0-zo.win_x))):
						#log("x", end="")
						if _x0 == zo.win_x : break
						_x0 += _x
						if zo.exit_thread: continue
						frame.place(x = _x0)
						time.sleep(BEEBOOT_SPEED)
						if zo.outside_labyrint and (zo.state == LABYRINT_MOVING_STATE or zo.state == LABYRINT_STATE):
							if _x0/LSPEED == int(_x0/LSPEED):	
								if zo.a[BACKWARD].cget('bg') ==OUTSIDE_LABYRINT_COLOR : 
									zo.a[BACKWARD].config(bg = bw_lbbg)
								else: zo.a[BACKWARD].config(bg = OUTSIDE_LABYRINT_COLOR)
					#log("")
					for _ in range(int(abs(_y0-zo.win_y))):
						#log("y", end="")
						if _y0 == zo.win_y : break
						_y0 += _y
						if zo.exit_thread: continue
						frame.place(y = _y0)
						time.sleep(BEEBOOT_SPEED)
						if zo.outside_labyrint and (zo.state == LABYRINT_MOVING_STATE or zo.state == LABYRINT_STATE):
							if _y0/LSPEED == int(_y0/LSPEED):	
								if zo.a[BACKWARD].cget('bg') ==OUTSIDE_LABYRINT_COLOR : 
									zo.a[BACKWARD].config(bg = bw_lbbg)
								else: zo.a[BACKWARD].config(bg = OUTSIDE_LABYRINT_COLOR)
					#log("")
					#log("2-_x, _x0, zo.win_x",_x, _x0, zo.win_x)
					#log("2-_y, _y0, zo.win_y",_y, _y0, zo.win_y)
					if zo.exit_thread: continue
					if check_position():
						zo.a[n].config(bg = OVER_TARGET_COLOR)
						time.sleep(0.1)
					elif zo.at_border == True:
						zo.a[n].config(bg = STOP_COLOR)
						if zo.sound_on:
							playSound(zo.stop_sound)
						zo.at_border = False
						time.sleep(0.3)
					else:
						zo.a[n].config(bg = BLINK_COLOR)
						if zo.sound_on:
							playSound(zo.start_sound)
						time.sleep(0.3)
					check_if_correct_target() # check target when moving forward or backward
				if zo.exit_thread: 
					zo.a[n].config(bg = lbbg)
					continue
				time.sleep(BEEBOOT_SPEED)
				
				zo.a[n].config(bg = lbbg)
				if zo.exit_thread: continue
				time.sleep(BEEBOOT_SPEED)
			#return the exit-flag to normal
			if not zo.exit_thread:
				zo.run_check_if_stopped_at_target = True # Start thread; App.check_if_stopped_at_target
			# Reset (set to False) of zo.exit_thread is done in "App.check_if_stopped_at_target"
			zo.run_start_begin_now = False

		log(("THREAD-start_begin_now OUT %s\n"%threading.current_thread().name ))
		
	def check_if_stopped_at_target(self, check_position, check_exercise):
		log(("THREAD-check_if_stopped_at_target IN %s\n"%threading.current_thread().name ))
		while True:
			while not zo.run_check_if_stopped_at_target: time.sleep(0.1) # wait until time to run the tread
			if zo.state == MATH_STATE:
				math_result = check_exercise()
			log(('wrong_target', zo.wrong_target))
			bw_lbbg = zo.a[BACKWARD].cget("bg")
			zo.stopped_at_target = True
			nmb_blink = 20
			if check_position() and not zo.wrong_target:
				if zo.sound_on and not zo.outside_labyrint:
					playSound(zo.goal_sound)
				while zo.stopped_at_target and nmb_blink:
					nmb_blink -=1
					#ws.Beep(zo.notes_array[zo.notes[20-nmb_blink]], 400)
					
					if zo.exit_thread: break
					for n in (LEFT, RIGHT): #FORWARD, BACKWARD,
						zo.a[n].config(bg = TARGET_FOUND_COLOR_1)
					if not zo.stopped_at_target : break
					if zo.exit_thread: break
					time.sleep(0.15)
					if zo.exit_thread: break
					if zo.outside_labyrint and (zo.state == LABYRINT_MOVING_STATE or zo.state == LABYRINT_STATE):
						zo.a[BACKWARD].config(bg = OUTSIDE_LABYRINT_COLOR)
					for n in (LEFT, RIGHT):#FORWARD, BACKWARD,
						#log(('zo.a', zo.a))
						zo.a[n].config(bg = TARGET_FOUND_COLOR_2)
					if not zo.stopped_at_target : break
					if zo.exit_thread: break
					time.sleep(0.15)
					if zo.outside_labyrint:
						zo.a[BACKWARD].config(bg = bw_lbbg)
			else: 
				if zo.sound_on:
					if zo.state == MATH_STATE and math_result == True:
						playSound(zo.goal_sound)
					else:
						playSound(zo.stop_sound)
					
			zo.exit_thread = False
			zo.run_check_if_stopped_at_target = False # Stop until flag is set
		log(("THREAD-check_if_stopped_at_target OUT %s\n"%threading.current_thread().name ))
	

top = tk.Tk()
geo = str(TOP_X_GEOMETRY)+'x'+str(TOP_Y_GEOMETRY)
top.withdraw()
top.update_idletasks()  # Update "requested size" from geometry manager
log (('top.winfo_screenwidth()',top.winfo_screenwidth()))
log (('top.winfo_screenheight()',top.winfo_screenheight()))

x = (top.winfo_screenwidth() - TOP_X_GEOMETRY) / 2
y = (top.winfo_screenheight() - TOP_Y_GEOMETRY) / 2
log (("%s+%d+%d" % (geo, x, y)))
top.geometry("%s+%d+%d" % (geo, x, y))
top.resizable(0,0)


# This seems to draw the window frame immediately, so only call deiconify()
# after setting correct window position
top.deiconify()

app = App(top)
top.mainloop()

#top.destroy()
